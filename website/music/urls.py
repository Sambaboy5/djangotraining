from django.urls import  re_path
from . import views

app_name = 'music'

urlpatterns = [
    # /musis/
    re_path(r'^$', views.IndexView.as_view(), name='index'),

    re_path(r'^register/$', views.UserFormView.as_view(), name='register'),

    # /musis/71/
    re_path(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),

    # /musis/album_id/favourite/
    re_path(r'^(?P<album_id>[0-9]+)/favorite/$', views.favorite, name='favorite'),

    # /music/album/add/
    re_path(r'album/add/$', views.AlbumCreate.as_view(), name='album_add'),

    # /music/album/2/
    re_path(r'album/(?P<pk>[0-9]+)/$', views.AlbumUpdate.as_view(), name='album_update'),

    # /music/album/delete /
    re_path(r'album/(?P<pk>[0-9]+)/delete/$', views.AlbumDelete.as_view(), name='album_delete'),
]