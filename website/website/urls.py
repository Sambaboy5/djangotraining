from django.urls import  re_path
from django.contrib import admin
from django.conf.urls import include
from django.conf import settings
from  django.conf.urls.static import static

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^music/', include('music.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_URL)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.MEDIA_ROOT)
